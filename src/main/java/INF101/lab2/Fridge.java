package INF101.lab2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    List<FridgeItem> FridgeList = new ArrayList<>();
    private int maxSize = 20;

    @Override
    public int nItemsInFridge() {
        return FridgeList.size();
    }
    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() < totalSize()){
            FridgeList.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(nItemsInFridge()==0) {
            throw new NoSuchElementException();
        }
        FridgeList.remove(item);

    }

    @Override
    public void emptyFridge() {
        FridgeList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFridgeList = new ArrayList<>();
        for(FridgeItem f : FridgeList){
            if(f.hasExpired()){
                expiredFridgeList.add(f);
            }
        }
        FridgeList.removeAll(expiredFridgeList);
        return expiredFridgeList;
    }
}
